FROM python:3.12-slim
WORKDIR /
RUN apt-get update
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
COPY . .
RUN pip install -r requirements.txt
EXPOSE 6001
CMD ["python", "-u", "main.py"]